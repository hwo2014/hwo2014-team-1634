package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import com.google.gson.Gson;

public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;

        String line = null;
        Boolean tryingToSwitch = false;
        Integer switchTo = 0; // Which slot are we trying to switch to
        Double throttle = .6; // Starting throttle
        Double angle = 0; // Most recent known slip angle of this car
        Object myCarsData = {}; // Holds the JSON for info about this car
        String myColor = "";

        send(join);

        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
			switch(msgFromServer.msgType) {
				case "carPositions":
                    if (tryingToSwitch) {
                        send(new Switch(switchTo));
                    } else {
						System.out.println(msgFromServer.data.getClass());
                        angle = msgFromServer.data[0].angle; // What the heck type of object is msgFromServer.data ? 
                        if (angle > 20) {
                            throttle -= .01;
                        } else if (throttle < 1) {
                            throttle += .01;
                        }
                        send(new Throttle(throttle));
                    }
					break;
					
				case "join":
					System.out.println("Joined");
					break;
					
				case "gameInit":
					System.out.println("Race init");
					break;
				
				case "gameEnd":
					System.out.println("Race end");
					break;
					
				case "gameStart":
					System.out.println("Race start");
					break;
				
				default:
					send(new Ping());
			}
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}

class Switch extends SendMsg {
    private int value;

    public Switch(int value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}