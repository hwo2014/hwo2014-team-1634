var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
  return send({
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });
});

function setThrottle(throttle) {
	send({
		msgType: "throttle",
		data: throttle
	});
}

function switchToSlot(index) {
	send({
		msgType: "switchLane",
		data: index
	});
}

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

var throttle = .6;

jsonStream.on('data', function(data) {
	switch(data.msgType) {
		// This is the most common game update tick
		case 'carPositions':
			var angle = data.data.angle;
			
			if (angle > 10) {
				throttle = 0;
			} else if (throttle < .9) {
				throttle += .01;
			}
			setThrottle(throttle); // .7 makes the car fly off the test track
			break;
		
		case 'join':
			console.log('Joined');
			console.log(data);
			break;
			
		case 'yourCar':
			color = data.data.color;
			break;
			
		// Gives us all of the track info
		case 'gameInit':
			var initData = data.data;
			console.log(initData);
			break;
			
		case 'gameStart':
			console.log('Race started');
			console.log(data);
			break;
			
		case 'gameEnd':
			console.log('Race ended');
			console.log(JSON.stringify(data));
			break;
			
		case 'tournamentEnd':
			break;
			
		// Somebody came off the track
		case 'crash':
			console.log('Somebody Crashed!!!');
			console.log(data.data);
			break;
			
		// Somebody got put back on the track
		case 'spawn':
			break;
			
		case 'lapFinished':
			console.log('Somebody finished a lap');
			console.log(JSON.stringify(data.data));
			break;
			
		// Somebody disconnected or errored out
		case 'dnf':
			break;
			
		// Somebody finished the race
		case 'finish':
			break;
			
		default: 
			console.log('We got a packet we don\'t recognize'); // All messages should be accounted for, shouldn't see this
			break;
	}
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});
